
from typing import List

def multiply_vector_by_vector(vector1: List, vector2: List) -> List:
    return [i * j for i, j in zip(vector1, vector2)]

def dot_product(vector1: List, vector2: List) -> float:
    return sum(multiply_vector_by_vector(vector1, vector2))

def generate_zero_vector(size: int) -> List:
    return [0 for i in range(size)]

def linear_evolve(probabilistic_operator: List[List], probabilistic_state: List) -> List:
    new_probabilistic_state = generate_zero_vector(len(probabilistic_operator))
    
    for i in range(len(probabilistic_operator)):
        new_probabilistic_state[i] = dot_product(probabilistic_operator[i], probabilistic_state)
    
    return new_probabilistic_state
